# README #

Описание API: https://github.com/Synerdocs/synerdocs-sdk/wiki/Введение
Более детальное описание проблемы тут: https://phpclub.ru/talk/threads/soap-некорректный-xml.86937/


При попытке совершить запрос GenerateTransportWaybillConsignorTitle не заполняются, или заполняются некорректно поля Consignee, Consignor, Requisites 
Эти поля имеют тип LegalEntityCounterparty и IndividualCounterparty, которые наследуются от CounterpartyBase. Но при формировании XML Soap-ом они либо не заполняются данными, либо заполняются данными родительского класса CounterpartyBase 
