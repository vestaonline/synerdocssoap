<?

/**
 * Класс пишущий числа словами. Поддерживает форматирование.
 * Пример:
 *
 * $iw = new Tools_SumToString($int); // $int - Это число, которое надо написать
 * echo $iw->get($patern);  // $patern - Строка с форматом результата, описание ниже.
 *
 * или короче:
 *
 * echo SumToString::getInstance($int)->get($patern);
 *
 * Патерн может быть следующий:
 *
 * 	$patern = "Всего [i/%d] ([i/%s]) [i~рубль|рубля|рублей~] [f/%02d] [f~копейка|копейки|копеек~]";
 *
 *  где первым стоит i или f
 * 		- i : целая часть
 * 		- f : дробная часть
 *
 *  после "/" идет формт выводимой строки описанный в sprintf , поддерживается d, f, s
 *
 *  [i~рубль|рубля|рублей~] - вывод строки, в зависимости от числа. *
 * 		- i : целая часть
 * 		- f : дробная часть
 *
 */

class Tools_SumToString
{
	// Массив, в котором будут храниться данные для формирования окончательной строки
	protected $array = array();

	// Здесь будут храниться число в разных своих состояниях
	protected $data = array(
		'full' => 0,  		// Полное десятичное число
		'integer' => 0, 	// Целая часть числа
		'fraction' => 0, 	// Дробная часть числа
		'integer_str' => '',	// Целая часть строкой
		'fraction_str' => ''	// Дробная часть строкой
	);

	public static function getInstance($sum)
	{
		$obj = new Tools_SumToString;
		$obj->parseSum($sum);
		return $obj;
	}

	public function get($patern)
	{
		$this->parsePatern($patern);
		$res = call_user_func_array("sprintf", $this->array);
		return $res;
	}

	public function parseSum($sum)
	{
		$tmp = explode(".", $sum);
		$this->data['full'] = $sum;
		$this->data['integer'] = $tmp[0];
		$this->data['fraction'] = (! empty($tmp[1]) ) ? $tmp[1] : 0 ;
		$this->data['integer_str'] = trim($this->toStr($this->data['integer']));
		$this->data['fraction_str'] = trim($this->toStr($this->data['fraction']));
	}

	public function parsePatern($patern)
	{
		$patern = preg_replace_callback("~\[.+\]~U", array($this, "callback"), $patern);
		array_unshift($this->array, $patern);
	}

	protected function callback($m)
	{
		if(preg_match("#\[(i|f)~([абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ|]+)~\]#i", $m[0], $matches) )
		{
			$tmp = explode("|", $matches[2]);
			switch($matches[1])
			{
				case "i" : return $this->declension($this->data['integer'], $tmp);
				case "f" : return $this->declension($this->data['fraction'], $tmp);
			}
		}


		if( preg_match('#\[(i|f)/%(.*(d|f|s))\]#i', $m[0], $matches) )
		{
			$i = count($this->array);

			if( $matches[1] == "i" )
			{
				switch($matches[3])
				{
					case "d":  $number = $this->data['integer']; break;
					case "f":  $number = $this->data['full']; break;
					case "s":  $number = $this->data['integer_str']; break;
				}

			}elseif( $matches[1] == "f" )
			{
				switch($matches[3])
				{
					case "d":  $number = $this->data['fraction']; break;
					case "f":  $number = $this->data['full']; break;
					case "s":  $number = $this->data['fraction_str']; break;
				}
			}

			$this->array[$i] = $number;
			return 	'%'. ($i+1) . '$' . $matches[2];
		}

		return "Error";
	}

	protected function toStr($sum)
	{
		$razrjad  = array('', 'тысяч', 'миллион');
		$array[0] = array('', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять', 'десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать');
		$array[1] = array('', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять', 'десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать');
		$array[2] = array('', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять', 'десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать');
		$array2   = array('', 'десять', 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто');
		$array3   = array('', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот');

		$oconchanie[0] = array('', '', '', '', '', '', '', '', '', '');
		$oconchanie[1] = array('', 'а', 'и', 'и', 'и', '', '', '', '', '');
		$oconchanie[2] = array('ов', '', 'а', 'а', 'а', 'ов', 'ов', 'ов', 'ов', 'ов');

		$length = ceil(strlen($sum)/3);
		$n = array();
		$v = 0;
		for($i=1; $i<=$length; $i++)
		{
			$start = max(0,strlen($sum) - 3*$i);
			$finish = min(3,3 -($i*3 - strlen($sum)));
			$str = substr($sum, $start, $finish);

			$p = array();

			$last_number = substr($str, -1, 1)*1;
			$last_two_numbers = substr($str, -2, 2)*1;
			$last_but_one = substr($str, -2, 1)*1;
			$first_number = substr($str, 0, 1)*1;

			if($last_two_numbers <20)
			{
				array_push($p, $array[$v][$last_two_numbers]);

			}else
			{
				array_push($p, $array2[$last_but_one], $array[$v][$last_number]);
			}

			if(strlen($str)==3)
			{
				array_unshift($p, $array3[$first_number]);
				array_push($p, $razrjad[$v].$oconchanie[$v][substr($str, -1, 1)*1]);

			}else
			{
				if($last_two_numbers<20 && $last_two_numbers > 10) array_push($p, $razrjad[$v]);
				else array_push($p, $razrjad[$v].$oconchanie[$v][$last_number]);
			}

			array_unshift($n, join(" ", $p));

			$v++;
		}

		$n = join(" ", $n);

		$n = trim($n) == '' ? "ноль" : $n;

		return $n;
	}




	protected function declension($int, $expressions)
    {
        if (count($expressions) < 3) $expressions[2] = $expressions[1];
        settype($int, "integer");
        $count = $int % 100;
        if ($count >= 5 && $count <= 20) {
            $result = $expressions['2'];
        } else {
            $count = $count % 10;
            if ($count == 1) {
                $result = $expressions['0'];
            } elseif ($count >= 2 && $count <= 4) {
                $result = $expressions['1'];
            } else {
                $result = $expressions['2'];
            }
        }
        return $result;
    }


}