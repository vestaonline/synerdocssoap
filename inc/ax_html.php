<?php
/**
 * Подготавливает форму для выбора даты в формате d.m.Y
 * @param $date
 * @param array $attr
 * @param array $opt
 * @return string
 */
function ax_input_date($date, $attr=[], $opt=[]){
    $opt = array_extend([
        'triggerBtn' => true,
        'format' => 'date'
    ], $opt);

    // Классы
    $class = '';
    if( ! empty($attr['class']) ){
        $class = $attr['class'];
        unset($attr['class']);
    }

    // атрибуты
    $attributes = [];
    foreach( $attr as $k=>$v ){
        $attributes[] = sprintf('%s="%s"', $k, $v );
    }
    $attributes = implode(' ', $attributes);

    // Обработка дат
    $arr = explode(' ', $date);
    $cdate = $arr[0];
    $ctime = ! empty($arr[1]) ? $arr[1] : '00:00:00';
    list($year, $month, $day) = explode('-', $cdate);
    list($hours, $minutes, $seconds) = explode(':', $ctime);


    $extra_classes = '';
    if( array_key_exists('readonly', $attr) ){
        $extra_classes .= ' ax-state-readonly';
    }
    if( array_key_exists('disabled', $attr) ){
        $extra_classes .= ' ax-state-disabled';
    }


    $time_tmpl = '';
    $input_type_class = 'ax-input-date';
    if( $opt['format'] == 'datetime' ){
        $input_type_class = 'ax-input-date-time';
        $time_tmpl = '<input type="text" class="ax-input-time-visible '.$extra_classes.'" value="'.$hours.':'.$minutes.'">';
    }

    $btn_tmpl = '';
    if( $opt['triggerBtn'] ){
        $btn_tmpl = '<a href="#" class="ax-input-date-trigger ax-button ax-button-18"><img src="/_img/no.gif" class="ax-icon-16 calendar"></a>';
    }

    $tmpl = '<span class="ax-date-container">'
        .'<input type="text" class="'.$input_type_class.' ax-input-date-hidden ax-input-prepared '.$class.'" value="'.$date.'" '.$attributes.'>'
        .'<input type="text" class="ax-input-date-visible '.$extra_classes.'" value="'.(implode('.', [$day,$month,$year])).'" />'.$time_tmpl.$btn_tmpl.'</span>';

    return $tmpl;
}

/**
 * Подготавливает форму для выбора даты в формате d.m.Y H:i
 * @param $date
 * @param array $attr
 * @param array $opt
 * @return string
 */
function ax_input_datetime($date, $attr=[], $opt=[]){
    $opt['format'] = 'datetime';
    return ax_input_date($date,$attr,$opt);
}



function ax_selectize($name){
    $html = '<div class="ax-selectize"></div>';

    return $html;
}