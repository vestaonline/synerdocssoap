<?php
/*******************************************************************************
* Функции, для работы с датами
* *****************************************************************************/

/**
* Функция преобразует даты из одного формата в другой
* работае только с числовыми датами.
* convertDateFormat($_date, $_format_from, $_format_to)
* */

function str2time($date, $format) {
    $search = array(	'd', 			'j', // day
                    	'm', 			'n', // month
                    	'Y', 			'y', // year
                    	'G', 			'H', // hour
                    	'i', 			's');

    $replace = array(	'(\d{2})', 		'(\d{1,2})', //day
                     	'(\d{2})', 		'(\d{1,2})', // month
                     	'(\d{4})', 		'(\d{2})', // year
                     	'(\d{1,2})',	'(\d{2})', // hour
                     	'(\d{2})', 		'(\d{2})');

	// Кэшируем даннык
    static $str_arr = array();
	if(isset($str_arr[$date])) return $str_arr[$date];


    $pattern = str_replace($search, $replace, $format);

    if(!preg_match("#$pattern#", $date, $matches)) return false;
    $dp = $matches;

    if(!preg_match_all('#(\w)#', $format, $matches)) return false;
    $id = $matches['1'];


    if(count($dp) != count($id)+1)  return false;

	$ret = array();

    for($i=0, $j=count($id); $i<$j; $i++) {
        $ret[$id[$i]] = $dp[$i+1];
    }

	$arr = array();

	// d
	if(! isset($ret['d']) ) $arr['d'] = isset($ret['j']) ? str_pad($ret['j'], 2, "0", STR_PAD_LEFT) : "00";
	else $arr['d'] = $ret['d'];

	// j
	if(! isset($ret['j']) ) $arr['j'] = isset($ret['d']) ? $ret['d']*1 : "0";
	else $arr['j'] = $ret['j'];

	// m
	if(! isset($ret['m']) ) $arr['m'] = isset($ret['n']) ? str_pad($ret['n'], 2, "0", STR_PAD_LEFT) : "00";
	else $arr['m'] = $ret['m'];

	// n
	if(! isset($ret['n']) ) $arr['n'] = isset($ret['m']) ? $ret['m']*1 : "0";
	else $arr['n'] = $ret['n'];

	// y
	if(! isset($ret['y']) ) $arr['y'] = isset($ret['Y']) ? substr($ret['Y'], -2) : "00";
	else $arr['y'] = $ret['y'];

	// Y
	if(! isset($ret['Y']) ) $arr['Y'] = isset($ret['y']) ? 2000 + $ret['y']*1 : "0000";
	else $arr['Y'] = $ret['Y'];

	// H
	if(! isset($ret['H']) ) $arr['H'] = isset($ret['G']) ? str_pad($ret['G'], 2, "0", STR_PAD_LEFT) : "00";
	else $arr['H'] = $ret['H'];

	// G
	if(! isset($ret['G']) ) $arr['G'] = isset($ret['H']) ? $ret['H']*1 : "0";
	else $arr['G'] = $ret['G'];

	// i
	if(! isset($ret['i']) ) $arr['i'] = "00";
	else $arr['i'] = $ret['i'];

	// s
	if(! isset($ret['s']) ) $arr['s'] = "00";
	else $arr['s'] = $ret['s'];


	//Записываем в кэш
	$str_arr[$date] = $arr;

	return $arr;
}

/**
*  Конвертируем из формата в формат
* */

function convertDateFormat($date, $from, $to)
{

	$res = str2time($date, $from);

	if( ! $res ) return null;

	$k = array_keys($res);
	$v = array_values($res);

	return str_replace($k, $v, $to);
}

/**
*  Конвертируем дату для SQL
* */
function dateToSQL($date, $format_from=null, $format_to=null)
{
	if(! $format_from ) $format_from = Config::read('App.Dates.date');
	if(! $format_to ) $format_to = "Y-m-d";

	return convertDateFormat($date, $format_from, $format_to);
}
// конвертит в формат sql
function dts($date, $format_from=null, $format_to=null)
{
    if( preg_match('~^\d{4}-\d{2}-\d{2}~', $date)){
        return $date;
    }
	return dateToSQL($date, $format_from, $format_to);
}


/**
*  Конвертируем дату и время для SQL
* */
function datetimeToSQL($date, $format_from=null, $format_to=null)
{
	if(! $format_from ) $format_from = Config::read('App.Dates.datetime');
	if(! $format_to ) $format_to = "Y-m-d H:i:s";

	return convertDateFormat($date, $format_from, $format_to);
}

function dtts($date, $format_from=null, $format_to=null){
	return datetimeToSQL($date, $format_from, $format_to);
}

/**
*  Конвертируем дату из SQL
* */
function dateFromSQL($date, $format_to=null, $format_from=null)
{
	if(! $format_from ) $format_from = "Y-m-d";
	if(! $format_to ) $format_to = 'd.m.Y';
	return convertDateFormat($date, $format_from, $format_to);
}

function dfs($date, $format_to=null, $format_from=null)
{
    if(! $format_to ) $format_to =  'd.m.Y';
    if( empty_date($date) ){
        return '';
    }
    $time = strtotime($date);
    return date($format_to, $time);
	//return dateFromSQL($date, $format_to, $format_from);
}

/**
*  Конвертируем дату и время из SQL
* */
function datetimeFromSQL($date, $format_to=null, $format_from=null)
{
	if(! $format_from ) $format_from = "Y-m-d H:i:s";
	if(! $format_to ) $format_to =  'd.m.Y H:i';

	$time = strtotime($date);
	return date($format_to, $time);

	//return convertDateFormat($date, $format_from, $format_to);
}

function  dtfs($date, $format_to=null, $format_from=null)
{
    if(! $format_to ) $format_to =  'd.m.Y H:i';
    if( empty_date($date) ){
        return '';
    }
    $time = strtotime($date);
    return date($format_to, $time);
}

function datetime_now($correct=null){
    $time = ! empty($correct) ? strtotime($correct) : time();
    return date('Y-m-d H:i:s', $time);
}

function date_now($correct=null){
    $time = ! empty($correct) ? strtotime($correct) : time();
    return date('Y-m-d', $time);
}

function datetime_empty(){
    return '0000-00-00 00:00:00';
}

function date_empty(){
    return '0000-00-00';
}

function date_to_timestamp($date, $default=0 ){
    $date = strtotime($date);
    return $date > 0 ? $date : $default;
}
function date_from_timestamp($time){
    $time_2000 = strtotime('2000-01-01 00:00:00');
    if( $time*1 < $time_2000 ){
        $date = '0000-00-00 00:00:00';
    }
    else{
        $date = date('Y-m-d H:i:s', $time);
    }
    return $date;
}

/**
 * Переводит время в фломате ЧЧ:ММ:СС в количество секунд
 * @param $time
 * @return int|null
 */
function time_to_sec($time){
    $arr = explode(':', $time);
    $h = opt($arr[0], 0)*60*60;
    $m = opt($arr[1], 0)*60;
    $s = opt($arr[2], 0);
    return ($h+$m+$s);

}


# Корректна ли дата для SQL
function validateSqlDate($date)
{
 	if(preg_match("~^[0-9]{4}-[0-9]{2}-[0-9]{2}.*$~", $date)) return true;
	return false;
}


# Выбираем месяц $n - номер месяца; $type - падеж месяца
# Если $n = 0, оттдается весть массив месяцев
function getMonth($n=0, $type=0)
{
	switch($type)
	{
	 	case 0:
			$months[1] = "Январь";
			$months[2] = "Февраль";
			$months[3] = "Март";
			$months[4] = "Апрель";
			$months[5] = "Май";
			$months[6] = "Июнь";
			$months[7] = "Июль";
			$months[8] = "Август";
			$months[9] = "Сентябрь";
			$months[10] = "Октябрь";
			$months[11] = "Ноябрь";
			$months[12] = "Декабрь";
			break;
	 	case 1:
			$months[1] = "Января";
			$months[2] = "Февраля";
			$months[3] = "Марта";
			$months[4] = "Апреля";
			$months[5] = "Мая";
			$months[6] = "Июня";
			$months[7] = "Июля";
			$months[8] = "Августа";
			$months[9] = "Сентября";
			$months[10] = "Октября";
			$months[11] = "Ноября";
			$months[12] = "Декабря";
			break;
                 case 2:
			$months[1] = "Янв";
			$months[2] = "Фев";
			$months[3] = "Мар";
			$months[4] = "Апр";
			$months[5] = "Май";
			$months[6] = "Июн";
			$months[7] = "Июл";
			$months[8] = "Авг";
			$months[9] = "Сен";
			$months[10] = "Окт";
			$months[11] = "Ноя";
			$months[12] = "Дек";
			break;
	}
	$n = $n*1;
	if($n) return $months[$n];
	return $months;
}


function dateDifference($date_start, $date_end, $format = "h")
{

    switch($format){
        case 'h':
            $dif = strtotime($date_end) - strtotime($date_start);
            $h = floor($dif/60/60);
            $m = floor( ($dif - $h*60*60)/60 );
            return sprintf ("%02d:%02d", $h, $m);

        case 'd':
            $date_end = substr($date_end, 0, 10);
            $date_start = substr($date_start, 0, 10);
            $dif = strtotime($date_end) - strtotime($date_start);
            $d = $dif/60/60/24;
            $d = $d > -1 && $d < 1 ? 0 : floor($d);
            return $d;
    }

	return false;
}

function getDayOfWeek($n, $type=0)
{
	switch($type)
	{
		case 0:
			$days[0] = "Воскресенье";
			$days[1] = "Понедельник";
			$days[2] = "Вторник";
			$days[3] = "Среда";
			$days[4] = "Четверг";
			$days[5] = "Пятница";
			$days[6] = "Суббота";
			break;
		case 1:
			$days[0] = "Вс";
			$days[1] = "Пн";
			$days[2] = "Вт";
			$days[3] = "Ср";
			$days[4] = "Чт";
			$days[5] = "Пт";
			$days[6] = "Сб";
			break;
	}
	$n = $n*1;
	return $days[$n];
}


# Список недель года
function listOfWeeks($year=null)
{
    $year = ! $year ? date("Y") : $year;

    if( empty($arr) ){
        static $arr = array();
    }

    if( ! empty( $arr[$year] ) ){
        return $arr[$year];
    }

    $arr[$year] = array();




    $i = 1;
    $week = date("W", mktime(0,0,0,1,$i,$year))*1;
    while($week != 1) $week = date("W", mktime(0,0,0,1,++$i,$year))*1;

    $fiest_day = $i;
    do{

        $w = $week;
        $arr[$year][$week]["first"] = date("Y-m-d", mktime(0,0,0,1,$fiest_day, $year));

        while($week == $w)
        {
            $fiest_day ++;
            $w = date("W", mktime(0,0,0,1,$fiest_day,$year))*1;
        }

        $arr[$year][$week]["last"] = date("Y-m-d", mktime(0,0,0,1,$fiest_day-1, $year));
        $week = $w;

    }while($week != 1);

    return $arr[$year];
}

# Отдает первй день недели
function firstDayOfWeek($week=null, $year=null, $real_day=false){

    $year =  (empty($year) ? date("Y") : $year)*1;
    $week = (empty($week) ? date('W') : $week)*1;


    $weeks = listOfWeeks($year);

    $first_day = $weeks[$week]['first'];
    $last_day = $weeks[$week]['last'];
    
    if( $week==1 && $real_day ){
        $first_day = date('Y-m-d', strtotime($last_day . '-6 days'));
    }

    return $first_day;
}

# Отдает последний день недели
function lastDayOfWeek($week=null, $year=null, $real_day=false){

    $year =  (empty($year) ? date("Y") : $year)*1;
    $week = (empty($week) ? date('W') : $week)*1;

    $weeks = listOfWeeks($year);

    $first_day = $weeks[$week]['first'];
    $last_day = $weeks[$week]['last'];

    if( $week==count($weeks)-1 && $real_day ){
        $last_day = date('Y-m-d', strtotime($first_day . '+6 days'));
    }

    return $last_day;
}



/**
* Функция, делающая из микротайма секунды
* */
function getmicrotime()
	{
    	list($usec, $sec) = explode(" ", microtime());
    	return ((float)$usec + (float)$sec);
	}




/**
 * Проверяем не пустая ли дата
 *
 * @param $str
 * @return bool
 */
function empty_date(&$str){
	if (empty($str)) {
		return true;
	}
	static $start_date = null;

	if( empty($start_date) ){
		$start_date = date('Y-m-d', 0);
	}

	if( preg_match('~^0000-00-00~', $str) || preg_match('~^'.$start_date.'~', $str) ){
	    return true;
	}
	return false;
}

/**
 * Проверяет является ли строка датой
 * @param $str
 * @return int
 */
function is_date($str){
    return preg_match('~^\d{4}-\d{2}-\d{2}~', $str);
}

/**
 * Добавляем или вычитаем из даты указанное количество дней, за вычетом дней недели считающихся выходными
 *
 * @param $date
 * @param $days
 * @param array $weekend
 * @return bool|string
 */
function date_add_days( $date, $days, $weekend = [ 0, 6 ] ){


    $znak = $days < 0 ? '-' : '+';
    $days = abs( $days );

    $date_ts = strtotime( $date );

    if( count($weekend) < 7 ){
        $i = 1;
        while( $i <= $days ){
            $date_ts = strtotime( $znak . ' 1 day', $date_ts );
            if( !in_array( date( 'w', $date_ts ), $weekend ) ){
                $i++;
            }
        }
    }

    $date = date( 'Y-m-d H:i:s', $date_ts );

    return $date;
}

/**
 * Проверяет входит ли дата в промежуток других дат
 *
 * @param $date
 * @param $date_start
 * @param $date_end
 * @return bool
 */
function date_is_between($date, $date_start, $date_end){
    $date = strtotime($date);
    $date_start = strtotime($date_start);
    $date_end = strtotime($date_end);

    return ( $date >= $date_start && $date <= $date_end );
}

function date_add_bank_days($days, $from=null, $format='Y-m-d'){
    include_once ( PATH_ROOT . '/_core/libs/Tools/BankDays.php');
    if(empty($from)) $from = date('Y-m-d');
    return BankDays::getEndDate($from, $days, $format);
}

function datetime_add_bank_days($days, $from=null, $format='Y-m-d H:i'){
    if(empty($from)) $from = date('Y-m-d H:i:s');
    return date_add_bank_days($from, $days, $format);
}

function date_add_calendar_days($days, $from=null, $format='Y-m-d'){
    if(empty($from)) $from = date('Y-m-d');
    $from = strtotime($from);
    $sec = $days * 24 * 60 * 60;
    return date($format, $from+$sec );
}

function datetime_add_calendar_days($days, $from=null, $format='Y-m-d H:i'){
    if(empty($from)) $from = date('Y-m-d H:i:s');
    return date_add_calendar_days($from, $days, $format);
}

function date_add_some_days($days, $days_type, $from=null, $format='Y-m-d'){
    if(empty($from)) $from = date('Y-m-d');
    switch($days_type){
        case 0: return date_add_bank_days($days, $from, $format);
        case 1: return date_add_calendar_days($days, $from, $format);
    }
    return $from;
}

function datetime_add_some_days($days, $days_type, $from=null, $format='Y-m-d H:i'){
    if(empty($from)) $from = date('Y-m-d H:i:s');
    switch($days_type){
        case 0: return datetime_add_bank_days($days, $from, $format);
        case 1: return datetime_add_calendar_days($days, $from, $format);
    }
    return $from;
}