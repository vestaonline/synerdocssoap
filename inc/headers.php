<?php
/*******************************************************************************
* Функции, оттдающие нужные заголовки
*******************************************************************************/


/**
* Заголовок, запрещающий кэширование страницы
* */
function headers_nocach($lastModifide=0)
{
	$lastModifide = ! $lastModifide ? strtotime('-1 day') : $lastModifide;

	header("{$_SERVER['SERVER_PROTOCOL']} 200 OK");

    # динамическая генерация даты позволит не "отпугнуть" роботов-индексаторов поисковых систем.
    	header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
   		header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $lastModifide) . ' GMT');

    # HTTP/1.1
    	header('Cache-Control: no-store, no-cache, must-revalidate');
    	header('Cache-Control: post-check=0, pre-check=0', false);
    	header('Cache-Control: max-age=0', false);

    # HTTP/1.0
    	header('Pragma: no-cache');
}


/**
*  Ошибка 404. Страница не найдена
* */
function headers_error404()
{
	header("{$_SERVER['SERVER_PROTOCOL']} 404 Not Found");
}

/**
*  Ошибка#401: У Вас нет доступа для просмотра данного раздел
* */
function headers_error401()
{
	header("{$_SERVER['SERVER_PROTOCOL']} 401 (Unauthorized) Authorization Required");
}


/**
* Заголовки MS Word
* */
function headers_word($filename)
{
   	header("Content-type: application/msword; charset=UTF-8");
   	header("Content-Disposition: attachment; filename=$filename" );
 	header("Expires: 0");
  	header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
   	header("Pragma: public");
}


/**
*  Заголовки MS Excel
* */
function header_excel($filename)
{
    header("Content-Type: application/x-msexcel; charset=UTF-8; format=attachment;");
    header("Content-Disposition: attachment; filename=$filename" );
   	header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
    header("Pragma: public");
}


function headers_file_download($filename, $mimetype='application/octet-stream') {

    // Отправляем требуемые заголовки
    header($_SERVER["SERVER_PROTOCOL"] . ' 200 OK');
    // Тип содержимого. Может быть взят из заголовков полученных от клиента
    // при закачке файла на сервер. Может быть получен при помощи расширения PHP Fileinfo.
    header('Content-Type: ' . $mimetype);
    // Дата последней модификации файла
    header('Last-Modified: ' . gmdate('r') );
    // Отправляем уникальный идентификатор документа,
    // значение которого меняется при его изменении.
    // В нижеприведенном коде вычисление этого заголовка производится так же,
    // как и в программном обеспечении сервера Apache
    //header('ETag: ' . sprintf('%x-%x-%x', fileinode($filename), filesize($filename), filemtime($filename)));
    // Размер файла
    //header('Content-Length: ' . (filesize($filename)));
    //header('Connection: close');
    // Имя файла, как он будет сохранен в браузере или в программе закачки.
    // Без этого заголовка будет использоваться базовое имя скрипта PHP.
    // Но этот заголовок не нужен, если вы используете mod_rewrite для
    // перенаправления запросов к серверу на PHP-скрипт
    header('Content-Disposition: attachment; filename="' . basename($filename) . '";');
}

function file_force_download($file, $unlink=false) {
    if (file_exists($file)) {
        // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
        // если этого не сделать файл будет читаться в память полностью!
        if (ob_get_level()) {
            ob_end_clean();
        }
        // заставляем браузер показать окно сохранения файла
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename($file));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        // читаем файл и отправляем его пользователю
        readfile($file);
        if( $unlink ){
            unlink($file);
        }
        exit;
    }
}


/**
 * Определяет запрошена ли страница через AJAX
 * */

/*
function isAjax()
{
	if( isAjah() || isJson() ) return true;
	return false;
}

function isAjah()
{

	static $res;
	if($res === true || $res === false ) return $res;
		$res = strstr( getenv('HTTP_ACCEPT'), "application/html+ajax") ? true : false;
	//	ppd(getenv('HTTP_ACCEPT'));
	return $res;
}

function isJson()
{
	static $res;

	if($res === true || $res === false ) return $res;
	$res = isset($_REQUEST["JsHttpRequest"]) ? true : false;

	return $res;
}
   */
   
function isAjax()
{
    if( isset( $_REQUEST["JsHttpRequest"] ) ){
        return true;
    }

    if( strstr(getenv('HTTP_ACCEPT'), "application/html+ajax") || array_key_exists('ajax', $_GET) ){
        return true;
    }

	if( function_exists('getallheaders'))
	{
		$allHeaders = getallheaders();
		$var_key = 'x-requested-with';
		$var_value = 'XMLHttpRequest';
	}
	else
	{
		$allHeaders = $_SERVER;
		$var_key = 'http_x_requested_with';
		$var_value = 'XMLHttpRequest';
	}

	foreach ( $allHeaders as $name => $value )
	{
		if ( strtolower($name) == $var_key && $value == $var_value)
		{
            return true;
		}
	}
	return false;
}

function http_header($code){

    $headers = [];
    foreach( apache_request_headers() as $k=>$v ){
        $headers[ strtoupper($k) ] = $v;
    }
    $code = strtoupper($code);

    if( isset($headers[$code]) ){
        return trim($headers[$code]);
    }
    return null;
}