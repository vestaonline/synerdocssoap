<?php
/*******************************************************************************
* Функции, помогающие работать с HTML
* *****************************************************************************/

/**
 * Функция создает тег <option>
 * Принемает значения:
 * $arr - Массив значений
 * $type - 	0: простой массив (значение массива заносится и в value и в Название
 * 			1: ассоциативный (в value заносится ключ, в название - значение
 * 			2: ассоциативный (в value это так же массив, а что является ключем и что значнием определяется в $opt
 * $selected - элемент по умолчанию, если $type==0 - значение, если $type==1 - ключ,
 * $opt - Массив настоек, [$key, $value, $attr], где
 *          $key - Элемент являющееся ключем
 *          $value - Элемент являющийся значением
 *          $attr - Список атрибутов <options>
 *
 * @param $arr
 * @param int $type
 * @param null $selected
 * @param array $opt
 * @return string
 */

	function makeSelectOptions($arr, $type=0, $selected=null, $opt=[])
	{
		$html = '';
		if(!is_array($arr) || !count($arr)) return $html;

		foreach ($arr as $key=>$value)
		{
			$key = is_numeric($key) ? $key*1 : $key;
			$selected = is_numeric($selected) ? $selected*1 : $selected;
			//$value = is_numeric($value) ? $value*1 : $value;

			if($type==0)
			{
                if( is_array($selected) ){
                    $s =  in_array($value, $selected) ? " selected" : "";
                }else{
                    $s =  $selected !== null && $selected === $value ? " selected" : "";
                }

				$html .= '<option value="'.$value.'"'.$s.'>'.$value.'</option>'."\n";
			}elseif($type==1)
			{

                if( is_array($selected) ){
                    $s =  in_array($key, $selected) ? " selected" : "";
                }else{
                    $s = $selected !== null && $selected === $key  ? " selected" : "";
                }

			//	pp("key = $key ; selected = $selected ; s = $s | ");
				$html .= '<option value="'.$key.'"'.$s.'>'.$value.'</option>'."\n";
			}elseif($type==2)
			{
                $k = $opt[0];
                $v = $opt[1];
                $a = ! empty($opt[2]) ? $opt[2] : [];

                $sel_key = $value[$k];
                $sel_val = $value[$v];

                $sel_key = is_numeric($sel_key) ? $sel_key*1 : $sel_key;

                if( is_array($selected) ){
                    $s =  in_array($sel_key, $selected) ? " selected" : "";
                }else{
                    $s = $selected !== null && $selected === $sel_key  ? " selected" : "";
                }

				$html .= '<option value="'.$key.'"'.$s.' ';

                foreach( $a as $attr ){
                    $html .= $attr .='="'. $value[$attr] .'" ';
                }

                $html .='>'.$sel_val.'</option>'."\n";
			}
		}
	//	die;
		return $html;
	}

	function mso($arr, $type=0, $selected=null, $opt=[])
	{
		return makeSelectOptions($arr, $type, $selected, $opt);
	}

	function mqs($old, $new=array()){
		return makeQueryString($old, $new);
	}

function url2str($url=null){
		$url = $url ? $url : getenv('REQUEST_URI');
		return urlencode($url);
	}


function sepByCol($arr, $num_col)
{
    $count = ceil(count($arr)/$num_col);

    $i = 0;
    $c = 0;

    $new = array();

    foreach ( $arr as $k=>$v)
    {
        if($i==$count){
            $i=0; $c++;
        }
        $new[$c][$i] = array($k,$v);
        $i++;
    }

    $arr = array();

    for($i=0; $i<$count; $i++){
        for($n=0; $n<$num_col; $n++){
            if(isset($new[$n][$i])){
                $arr[$new[$n][$i][0]]= $new[$n][$i][1];
            }
        }
    }

    return $arr;
}

/**
 * В зависимости от условия проставляет атрибут "checked" для input:checkbox
 * @param $condition
 * @return string
 */
function chbch( $condition ){
    return $condition ? 'checked' : '';
}

function adis($condition){
    return $condition ? 'disabled="disabled"' : '';
}


/**
 * Добавляет атрибуты сортировки
 *
 * @param $field
 * @param $sorted_by
 * @param $sorted_type
 * @param string $extra_classes
 * @return string
 */
function sorting_attr($field, $sorted_by, $sorted_type, $extra_classes=''){
    $classes = trim($extra_classes);
    $classes .= ! empty($classes) ? ' ' : '';
    $classes .= 'sorting';

    $sorted_type = strtoupper($sorted_type);

    if( $sorted_by == $field ){
        $classes .= (' sorted-'.$sorted_type);
        $sorted_type = ( $sorted_type == 'ASC' ? 'DESC' : 'ASC');
    }else{
        $sorted_type = 'ASC';
    }

    $str = 'class="%s" data-col="%s" data-type="%s"';
    return sprintf($str, $classes, $field, $sorted_type);
}


function dp_name_decorate($city,$net,$dp,$address='', $search='', $wrap=[]){

    if( ! empty($dp) ){
        $dp = str_replace([',', '(', ')'],[', ', ' (', ') '],$dp);
    }

    if( ! empty($search) ){
        if( ! empty($dp) ){
            $dp = html_highlight($search,$dp);
        }
        if( ! empty($address) ){
            $address = html_highlight($search,$address);
        }

    }

    $arr = [];
    if( ! empty($city) ){
        $arr[] = sprintf('<span class="city_name">%s</span>', $city);
    }
    if( ! empty($net) ){
        $arr[] = sprintf('<span class="net_name">%s</span>', $net);
    }
    if( ! empty($dp) ){
        $arr[] = sprintf('<span class="dp_name">%s</span>', $dp);
    }
    if( ! empty($address) ){
        $address = sprintf('<div class="address">%s</div>', $address);
    }
    
    $str_dp = implode('<span class="sep">::</span>', $arr);
    
    if( ! empty($wrap) ){
        $tag = array_shift($wrap);
        $attr = [];
        foreach( $wrap as $k=>$v ){
            $attr[] = sprintf('%s="%s"', $k, hh($v));
        }
        $str_dp = sprintf('<%s %s>%s</%s>', $tag, implode(' ', $attr), $str_dp, $tag);
    }
    

    return sprintf(
        '<span class="decorator dp">%s%s</span>',
        $str_dp,
        $address
    );
}

function car_number_decorate($number){
    $root = '<span class="decorator car_number">%s</span>';
    $number = preg_replace('~([0-9]+)~', '<span class="numeric">$1</span>', $number);
    $number = sprintf($root, $number);
    return $number;
}

function html_highlight($search, $text){
    if( trim($search) == '' ){
        return $text;
    }
//    $search = u2w($search);
//    $text = u2w($text);
    $result = preg_replace('~('.($search).')~ui', '<b class="highlight">$1</b>', $text);
    return ($result);
    //return w2u($result);
}

/**
 * Пишет аттрибуты HTML
 * @param $attr
 * @return string
 */
function html_attributes($attr){
    $res = [];

    foreach( $attr as $k=>$v ){
        $res[] = sprintf('%s="%s"', $k, hh($v));
    }

    return implode(' ', $res);
}