<?php
/*******************************************************************************
* Другие полезные функции, не вошедшие в другие файлы
* *****************************************************************************/


///////////////////////////////////////////////////////////////////////////////
// Функция, которая чистит весь буфер, дополнение к ф-ям ob_                 //
///////////////////////////////////////////////////////////////////////////////

	function ob_clean_all()
	{
		while(ob_get_level()) ob_end_clean();
	}



/**
* Функция печатает массив обернутый в <pre></pre>
* Для разных отладочных моментов
* */
	function pp ($arr)
	{
		if(is_array($arr))
		{
			print "<pre>\n";
			print_r($arr);
			print "</pre>\n";
		}else
		{
		 print "<p>$arr</p>\n";
		}
	}

	function ppd($arr)
	{
		pp ($arr);
		die;
	}

	function ppr($arr)
	{
		ob_start();
		pp ($arr);
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}

	function pps(){
	    $args = func_get_args();
	    $str = call_user_func_array('sprintf', $args);
	    pp($str);
    }

	function ppsr(){
	    $args = func_get_args();
	    $str = call_user_func_array('sprintf', $args);
	    return ppr($str);
    }

	function pptbl(){
        $args = func_get_args();
        $return = '<table border="1" cellspacing="10"><tbod><tr>%s</tr></tbod></table>';
        $tds = [];
        foreach( $args as $v ){
            $tds[] = '<td valign="top">'. ppr($v) .'</td>';
        }

        print sprintf($return, implode('', $tds));
    }

    function ppta($data){
        ob_start();
        if(is_array($data)){
            print_r($data);
        }else{
            print $data;
        }
        $html = ob_get_contents();
        ob_end_clean();
        $count_rows = count(explode("\n", $html));
        $count_rows = max($count_rows, 5);
        $count_rows = min($count_rows, 40);
        print '<textarea rows="'. $count_rows.'" style="width: 100%;">'.hh($html).'</textarea>';
    }

	function err()
	{
		$args = func_get_args();
		//ppd($args);
		if(count($args)>1){
			$msg = @ call_user_func_array('sprintf', $args);
		}else{
			$msg = $args[0];
		}

        if( isAjax() ){
            die($msg);
        }

        $level = ob_get_level();
        while (ob_get_level() > $level){
            ob_end_clean();
        }

        $customise_templates_dir = PATH_CORE . DS . PATH_CUSTOMIZE . DS . SP_VERSION . DS. PATH_TEMPLATES  . DS;
        $default_templates_dir = PATH_CORE . DS . PATH_TEMPLATES . DS;

        if (file_exists( $customise_templates_dir . "error.php")) {
            $page_template = $customise_templates_dir . "error.php";
        } else {
            $page_template = $default_templates_dir . "error.php";
        }

        $tmpl = new View();
        $tmpl->Template = $page_template;
        $tmpl->set("result", $msg);

        $tmpl->set("FOOTER_COPYRIGHT", Config::read("App.Old.footer_copyright"));
        $tmpl->set("SUPPORT", Config::read("App.Old.support"));
        $tmpl->set("FOOTER_CONTACTS", Config::read("App.Old.footer_contacts"));
        $tmpl->set("FOOTER_PRODUCER", Config::read("App.Old.footer_producer"));

        $html = $tmpl->getResult();
        header("Content-type: text/html; charset=utf-8");
        print $html;
        die;
    }


	/**
	* функции для получения дефолтного значения при отсутсвии переменной
	* */
	function opt(& $var, $default=null)
	{
		if( empty($var) ) return $default;
		return $var;
	}


/*******************************************************************************
* Функция создает строку QueryString
* Получает на входе два массива
* 		$old - 	Массив уже существующих переменны
* 		$new - 	Новый масив. Если в данном массиве находятся переменные
* 				существующие в $old, то они перезаписываются, если нет,
* 				добавляются, если существует, но == null, удаляются
* *****************************************************************************/

	function makeQueryString($old, $new)
	{
		$arr = mergeTwoArrays($old, $new);
		$str = "?". http_build_query($arr);
		return $str;
	}


	# Обединяет рекурсивно два массива.
	function mergeTwoArrays($old, $new)
	{
		foreach($new as $key=>$value)
		{
			if(isset($old[$key]))
			{
				if($value === null) unset($old[$key]);
				else
				{
					if(is_array($value))
					{
						$old[$key] = mergeTwoArrays($old[$key], $value);
					}else
					{
						$old[$key] = $value;
					}
				}
			}else
			{
				if($value) $old[$key] = $value;
			}
		}

		return $old;
	}



/*******************************************************************************
* Функция для обработки ошибок базы данных
* *****************************************************************************/

	function databaseErrorHandler($message, $info)
	{
    	/*
		$f=fopen("log.txt", "w");
    	fwrite($f, join( "\n", $info));
    	fclose($f);
		*/

		// Если использовалась @, ничего не делать.
   		if (!error_reporting()) return;
    	// Выводим подробную информацию об ошибке.
		print "<p>SQL Error</p>";
		print "<pre>";
		print_r($info);
		print "<pre>";
		die;
	}

/*******************************************************************************
* Если не определена функция кодирования в JSON, то определяем ее
* *****************************************************************************/

function my_json_encode($a=false)
{

	if (is_null($a)) return 'null';
	if ($a === false) return 'false';
	if ($a === true) return 'true';
	if (is_scalar($a))
	{
		if (is_float($a))
		{
			// Always use "." for floats.
			return floatval(str_replace(",", ".", strval($a)));
		}

		if (is_string($a))
		{
			static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
			return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $a) . '"';
		}
		else
			return $a;
	}

	$isList = true;
	for ($i = 0, reset($a); $i < count($a); $i++, next($a))
	{
		if (key($a) !== $i)
		{
			$isList = false;
			break;
		}
	}
	$result = array();
	if ($isList)
	{
		foreach ($a as $v) $result[] = my_json_encode($v);
		return '[' . join(',', $result) . ']';
	}
	else
	{
		foreach ($a as $k => $v) $result[] = my_json_encode($k).':'.my_json_encode($v);
		return '{' . join(',', $result) . '}';
	}
}


function arr_val($arr,$key,$default=null){
    return array_key_exists($key, $arr) ? $arr[$key] : $default;
}

function test_var_type($var){
    if( is_array($var) ){
        return 'array';
    }
    if( is_object($var) ){
        return 'object';
    }
    if( is_null($var) ){
        return 'null';
    }
    if( is_bool($var) ){
        return 'bool';
    }
    if( is_numeric($var) ){
        if( is_float($var+0) ){
            return 'float';
        }
        if( is_int($var+0) ){
            return 'int';
        }
    }

    if( is_string($var) ){
        return 'string';
    }
    return 'string';
}

/**
 * Функция заполняет ассациативный массив значениями, так, что если в первом есть
 * ключь, то изменяется, если нет то не изменяется
 *
 * $save_types - сохраняет тип перменной.
 * */
function array_extend($arr_1, $arr_2, $save_types=false, $recursive=false )
{
    if(empty($arr_2)) return $arr_1;

    foreach($arr_2 as $k=>$v)
    {
        if(array_key_exists($k, $arr_1)){
            if ($save_types) {
                if (is_integer($arr_1[$k])) $v = (int) $v;
                if (is_bool($arr_1[$k]))  	$v = (bool) $v;
                if (is_float($arr_1[$k]))  	$v = (float) $v;
                if (is_string($arr_1[$k]))  $v = (string) $v;
                if (is_array($arr_1[$k])){
                    if( empty($v) ){
                        $v = array();
                    }else{
                        $v = (array) $v;
                        if( $recursive ){
                            $arr_keys = array_keys( $v );
                            if( is_string( $arr_keys[0] ) && !empty( $arr_1[$k] ) ){
                                $v = array_extend($arr_1[$k], $v, true, true);
                            }
                        }
                    }
                };
            }
            $arr_1[$k] = $v;
        }
    }

    return $arr_1;
}


function array_clean_empty($arr){
    foreach( $arr as $k=>$v ){
        if( empty($v) ){
            unset($arr[$k]);
        }
    }
    return $arr;
}

/**
 * Функция переносит выбранные элементы вначало масстива
 * @param $arr
 * @param $selected
 * @return array
 */
function array_to_start($arr, $selected){
    $tmp = [];

    foreach( $selected as $k ){
        if( isset($arr[$k]) ){
            $tmp[$k] = $arr[$k];
            unset($arr[$k]);
        }
    }

    foreach( $arr as $k=>$v ){
        $tmp[$k] = $v;
    }

    return $tmp;
}

function array_make_list($arr=array(), $key=null, $value){
	$list = array();
	if (! empty($arr) ) {
		foreach ($arr as $v ) {
			if($key !== null ) $list[$v[$key]] = $v[$value];
			else $list[] = $v[$value];
		}
	}

	return $list;
}

function pages_list($count, $on_page){
	$pages = array();
	for($i=0; $i<ceil($count/$on_page); $i++){
		$pages[$i] = $i+1;
	}
	return $pages;
}

/**
 * Функция определяет различаются ли 2 массива.
 * Если да - true, нет - false
 * Третим параметром массив из ключей которые надо сравнивать.
 * Если ключи не определены, сравнивает весь массив
 *
 * @param mixed $array1
 * @param mixed $array2
 * @param array $keys
 * @return
 */
function array_is_diff($array1, $array2, $keys=array() ){

	if (! empty($array1) && ! empty($array2)) {
		foreach ($array1 as $k=>$v ) {
			if (! empty($keys) && ! in_array($k, $keys)) {
				continue;
			}

			if (! array_key_exists($k, $array2)) {
				return true;
			}

			if ($v != $array2[$k]) {
				return true;
			}
		}

		return false;
	}
	return true;
}


function like_prepare($str)
{
	return strtr($str, array("'"=>"", '"'=>"", "%"=>"", "*"=>"%"));
}


/**
 * Функция отдает высоту строки экселя в зависимости от количества символов
 * Принемает массив колонок, каждая из которых определяется следующими свойствами:
 * height - Высота одной строки
 * length - Максимальное количество символом помещающихся в строку.
 * str - Собственно строка
 *
 * Пример: array(
 *		array( 'height'=>14.25, 'lenght'=>35, str='...'),
 *		array( 'height'=>14.25, 'lenght'=>20, str='...'),
 * )
 *
 * $encode='WIN' или 'UTF' - В какой кодировке приходят данные
 *
 * @param array $params
 * @return
 */
function excel_row_height($params, $encode='UTF-8'){
	$row_height = 0;
	if (! empty($params)) {
		foreach ($params as $k=>$v ) {
			$str = $encode=='WIN' ? $v['str'] : u2w( $v['str'] );
			$length = strlen($str)+1;
			$height = ceil($length/$v['lenght'])*$v['height'];
			if ($height>$row_height) {
				$row_height = $height;
			}
		}
	}
	return min($row_height, 400);
}


function real_path($path){
    if( ! file_exists($path) ){
        $old = umask(0);
        mkdir($path, 0777, true);
        umask($old);
    }
    return $path;
}

function filesize2bytes($bytes) {
    $bytes = floatval($bytes);
    $arBytes = array(
        0 => array(
            "UNIT" => "TB",
            "VALUE" => pow(1024, 4)
        ),
        1 => array(
            "UNIT" => "GB",
            "VALUE" => pow(1024, 3)
        ),
        2 => array(
            "UNIT" => "MB",
            "VALUE" => pow(1024, 2)
        ),
        3 => array(
            "UNIT" => "KB",
            "VALUE" => 1024
        ),
        4 => array(
            "UNIT" => "B",
            "VALUE" => 1
        ),
    );

    $result = '';
    foreach($arBytes as $arItem)
    {
        if($bytes >= $arItem["VALUE"])
        {
            $result = $bytes / $arItem["VALUE"];
            $result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
            break;
        }
    }
    return $result;
}

class DBG{
    protected static $start = 0;
    protected static $current = 0;
    public static $enable = false;
    protected static $log = array();

    static function timerStart($enable=false){
        self::$enable = $enable;
        self::$start = microtime(true);
        self::$current = self::$start;
        if( $enable ){
            @set_time_limit(0);
            @session_write_close();
            @header('X-Accel-Buffering: no'); 
            @ob_end_flush();
        }
    }
    
    protected static function logIn($str){
        self::$log[] = $str;
    }
    
    static function logOut(){
        foreach( self::$log as $log){
            pp($log);
        }
    }

    static function timerFix($str){
        $mt = microtime(true);
        $cr = self::$current;
        $str = sprintf('%05.3f; total: %s - %s', self::secToStr($mt-$cr), self::secToStr($mt-self::$start), $str);
        self::logIn($str);

        if( self::$enable ){
            pp($str);
            flush();
        }
        self::$current = $mt;
    }

    static function timerEnd($str){
        self::timerFix($str);
        $mt = self::$current;
        $cr = self::$start;
        if( self::$enable ){
            ppd(sprintf('Всего затрачено - %s', self::secToStr($mt-$cr)));
            flush();
            exit;
        }
    }

    protected static function secToStr($time){

        $sec_in_year = (60*60*24*365);
        $sec_in_month = (60*60*24*30.41);
        $sec_in_week = (60*60*24*7);
        $sec_in_day = (60*60*24);
        $sec_in_hour = (60*60);
        $sec_in_min = (60);

        $years  = floor($time/$sec_in_year);
        $months = floor(($time - $years*$sec_in_year)/$sec_in_month);
        $weeks  = floor(($time - $years*$sec_in_year - $months*$sec_in_month)/$sec_in_week);
        $days   = floor(($time - $years*$sec_in_year - $months*$sec_in_month - $weeks*$sec_in_week)/$sec_in_day);
        $hours  = floor(($time - $years*$sec_in_year - $months*$sec_in_month - $weeks*$sec_in_week - $days*$sec_in_day)/$sec_in_hour);
        $minute = floor(($time - $years*$sec_in_year - $months*$sec_in_month - $weeks*$sec_in_week - $days*$sec_in_day - $hours*$sec_in_hour)/$sec_in_min);
        $seconds= round(($time - $years*$sec_in_year - $months*$sec_in_month - $weeks*$sec_in_week - $days*$sec_in_day - $hours*$sec_in_hour - $minute*$sec_in_min), 3);

        $str = [];
        if( ! empty($years) ){
            $str[] = "{$years} y";
        }
        if( ! empty($months) ){
            $str[] = "{$months} m";
        }
        if( ! empty($weeks) ){
            $str[] = "{$weeks} w";
        }
        if( ! empty($days) ){
            $str[] = "{$days} d";
        }
        if( ! empty($hours) ){
            $str[] = "{$hours} h";
        }
        if( ! empty($minute) ){
            $str[] = "{$minute} min";
        }
        if( ! empty($seconds) ){
            $str[] = "{$seconds} sec";
        }

        return implode(' ', $str);
    }
}

class Sorter{
    public static function sort($arr, $keys){
        usort($arr, self::build_sorter($keys));
        return $arr;
    }
    public static function asort($arr, $keys){
        uasort($arr, self::build_sorter($keys));
        return $arr;
    }

    public static function build_sorter($keys){
        return function($a,$b)use ($keys){
            for($i=0; $i<count($keys); $i++){
                $aa = self::value_by_key($a, $keys[$i]);
                $bb = self::value_by_key($b, $keys[$i]);
                $res = strnatcasecmp(u2w($aa),u2w($bb));
                if( $res != 0 ){
                    return $res;
                }
            }
            return $res;
        };
    }

    protected static function value_by_key($arr,$key){
        $k = explode('.',$key);
        $count = count($k);
        switch($count){
            case 1: return isset($arr[$k[0]])                               ? $arr[$k[0]] : 0;
            case 2: return isset($arr[$k[0]][$k[1]])                        ? $arr[$k[0]][$k[1]] : 0;
            case 3: return isset($arr[$k[0]][$k[1]][$k[2]])                 ? $arr[$k[0]][$k[1]][$k[2]] : 0;
            case 4: return isset($arr[$k[0]][$k[1]][$k[2]][$k[3]])          ? $arr[$k[0]][$k[1]][$k[2]][$k[3]] : 0;
            case 5: return isset($arr[$k[0]][$k[1]][$k[2]][$k[3]][$k[4]])   ? $arr[$k[0]][$k[1]][$k[2]][$k[3]][$k[4]] : 0;
            default: return 0;
        }
    }
}

/**
 * Сортирует массив в порядке возрастания без сохранения ключей
 *
 * @param $arr
 * @param $keys
 * @param string $type (ASC || DESC)
 * @return mixed
 */
function array_sort($arr, $keys, $type='ASC'){
    if( ! is_array($keys) ){
        $keys = [$keys];
    }

    $arr = Sorter::sort($arr, $keys);

    if( $type == 'DESC' ){
        $arr = array_reverse($arr);
    }

    return $arr;
}

/**
 *  Сортирует массив в порядке возрастания с сохранением ключей
 *
 * @param $arr
 * @param $keys
 * @param string $type (ASC || DESC)
 * @return mixed
 */
function array_asort($arr, $keys, $type='ASC'){
    if( ! is_array($keys) ){
        $keys = [$keys];
    }

    $arr = Sorter::asort($arr, $keys);

    if( $type == 'DESC' ){
        $arr = array_reverse($arr, true);
    }
    return $arr;
}


/**
 * Создает массив данных их указанных элементов массива
 *
 * @param $arr
 * @param $key
 * @return array
 */
function array_to_list($arr, $key){
    $new = [];
    foreach( $arr as $k=>$v ){
        $new[] = $v[$key];
    }
    return $new;
}

/**
 * Добавляет $new в массив $arr. Если $new является массивом, то каждый его элемент 
 * добавляется $arr как новый элемент массива $arr
 * @param & $arr_1
 * @param $arr_2
 */
function array_add(&$arr, $new){
    if( is_array($new) ){
        foreach( $new as $k=>$v ){
            $arr[] = $v;
        }
    }
    else{
        $arr[] = $new; 
    }    
}

/**
 * Определяет является ли массив ассоциативным
 * @param $arr
 * @return bool
 */
function array_is_assoc($arr){
    $keys = array_keys($arr);
    for($i=0; $i<count($keys); $i++ ){
        if( $i !== $keys[$i] ){
            return true;
        }
    }
    return false;
}

/**
 * Добавляет массив в конец массива.
 * Если начальный массив является ассоциативным или третий элемент явно на это указывает, то ключи сохраняются,
 * если нет - то ключи добавляются.
 * @param $old
 * @param $new
 * @param null $is_assoc
 * @return array
 */
function array_append($old, $new, $is_assoc=null){
    if( $is_assoc === null ){
        $is_assoc = array_is_assoc($old);
    }
    foreach( $new as $k=>$v ){
        if( $is_assoc ){
            $old[$k] = $v;
        }
        else{
            $old[] = $v;
        }
    }
    return $old;
}

/**
 * Добавляет массив в начало массива.
 * Если начальный массив является ассоциативным или третий элемент явно на это указывает, то ключи сохраняются,
 * если нет - то ключи изменяются.
 * @param $old
 * @param $new
 * @param null $is_assoc
 * @return array
 */
function array_prepend($old, $new, $is_assoc=null){
    if( $is_assoc === null ){
        $is_assoc = array_is_assoc($old);
    }
    $res = [];
    foreach( $new as $k=>$v ){
        if( $is_assoc ){
            $res[$k] = $v;
        }
        else{
            $res[] = $v;
        }
    }
    foreach( $old as $k=>$v ){
        if( $is_assoc ){
            $res[$k] = $v;
        }
        else{
            $res[] = $v;
        }
    }
    return $res;
}


/**
 * Возвращает первый элемент из массива
 *
 * @param $arr
 * @return null
 */
function array_first($arr){
    if( empty($arr) ){
        return null;
    }
    $keys = array_keys($arr);
    $first_key = $keys[0];
    return $arr[$first_key];
}

/**
 * Преобразует объект в массив
 * @param $data
 * @return array
 */
function object_to_array($data)
{
    if (is_array($data) || is_object($data))
    {
        $result = array();
        foreach ($data as $key => $value)
        {
            $result[$key] = object_to_array($value);
        }
        return $result;
    }
    return $data;
}

function get_supplier($arr,$wrap=true){
    if( empty($arr['real_customer_name']) ){
        return '';
    }

    if( $wrap ){
        return ' / ' . "<span class=\"customer_supplier\">{$arr['real_customer_name']}</span>";
    }

    return ' / ' . $arr['real_customer_name'];

}

function gs($arr,$wrap=true){
    return get_supplier($arr,$wrap);
}

/**
 * basename - режет русские буквы. Надо смотреть в сторону локали
 *
 * @param $file
 * @return array|mixed
 */
function file_basename($file){
    $file = str_replace('\\', '/', $file);
    $basename = explode('/', $file);
    $basename = array_pop($basename);
    return $basename;
}

function file_get_contents_curl( $url, $post=[], & $info=[] ) {
    $ch = curl_init();

    curl_setopt( $ch, CURLOPT_AUTOREFERER, TRUE );
    curl_setopt( $ch, CURLOPT_HEADER, 0 );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt( $ch, CURLOPT_URL, $url );
    curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, TRUE );
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );

    $data = curl_exec( $ch );
    $info = curl_getinfo($ch);

    curl_close( $ch );
    return $data;

}

/**
 * Расстояние между двумя точками в метрах
 *
 * @param $point_1 Широта и долгота 1-й точки:  Array('lat'=>n, 'lng'=>n);
 * @param $point_2 Широта и долгота 2-й точки:  Array('lat'=>n, 'lng'=>n);
 * @return float|int
 */
function distance_by_coordinates($point_1, $point_2){
    $EARTH_RADIUS = 6372795; // Радиус земли в метрах

    // перевести координаты в радианы
    $lat1 = $point_1['lat'] * M_PI / 180;
    $lat2 = $point_2['lat'] * M_PI / 180;
    $long1 = $point_1['lng'] * M_PI / 180;
    $long2 = $point_2['lng'] * M_PI / 180;

    // косинусы и синусы широт и разницы долгот
    $cl1 = cos($lat1);
    $cl2 = cos($lat2);
    $sl1 = sin($lat1);
    $sl2 = sin($lat2);
    $delta = $long2 - $long1;
    $cdelta = cos($delta);
    $sdelta = sin($delta);

    // вычисления длины большого круга
    $y = sqrt(pow($cl2 * $sdelta, 2) + pow($cl1 * $sl2 - $sl1 * $cl2 * $cdelta, 2));
    $x = $sl1 * $sl2 + $cl1 * $cl2 * $cdelta;

    $ad = atan2($y, $x);
    $dist = $ad * $EARTH_RADIUS;

    return $dist;
}

/**
 * Отдает HTTP или HTTPS
 * @return string
 */
function http_scheme(){
    $scheme = 'https';

    do{
        if( isset($_SERVER['HTTP_SCHEME']) ){
            $scheme = $_SERVER['HTTP_SCHEME'];
            break;
        }
        if( isset($_SERVER['REQUEST_SCHEME']) ){
            $scheme = $_SERVER['REQUEST_SCHEME'];
            break;
        }
    }while(false);

    return $scheme;

}