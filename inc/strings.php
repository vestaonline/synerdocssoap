<?php
/*******************************************************************************
* Функции для работы со строками
* *****************************************************************************/

/**
* Набор функций, переводящих регистр букв.
* Не зависит от локали
* */
	function registerArr()
	{
		static $arr = array(
		"АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧЪЫЬЭЮЯABCDEFGHIJKLMNOPQRSTUVWXYZ",
		"абвгдеёжзийклмнопрстуфхцчъыьэюяabcdefghijklmnopqrstuvwxyz"
		);
		return $arr;
	}

	function toLower($str)
	{
		$arr = registerArr();
		$str = strtr($str, $arr[0], $arr[1]);
		return $str;
	}

	function toUpper($str)
	{
		$arr = registerArr();
		$str = strtr($str, $arr[1], $arr[0]);
		return $str;
	}

	function firstToUpper($str)
	{
		$st = substr($str, 1);
		$char = substr($str, 0, 1);
		$char = toUpper($char);
		return $char.$st;
	}

/**
* Функция объединяет строки
* Первым параметром идет сапаратор, затем строки через запятую
* */
	function str_join ()
	{
		$arr = func_get_args();
		$sep = $arr[0];
		unset($arr[0]);
		return join($sep, $arr);
	}


/***
* Функция, подготавливающая строку для поиска с помощью LIKE
* */

	function likePrepare($str)
	{
		return strtr(trim($str), array("'"=>"", '"'=>"", "%"=>"", "*"=>"%"));
	}

/**
* Функции, генерирующие произвольные строки или числа
* */

	function get_random_key($keylength=8) {
    	$alphabet = 'abcdefghijkmnopqrstuvwxyz1234567890ABCDEFGHJKLMNPQRSTUVWXYZ';
    	$alphalen = strlen($alphabet);
    	$keys = '';
    	mt_srand((double)microtime()*1000000);
    	for($i=0;$i<$keylength;$i++) $keys .= substr($alphabet, mt_rand() % $alphalen, 1);
    	return $keys;
  	}

  	function get_random_number($keylength=8) {
    	$alphabet = '123456789';
    	$alphalen = strlen($alphabet);
    	$keys = '';
    	mt_srand((double)microtime()*1000000);
    	for($i=0;$i<$keylength;$i++) $keys .= substr($alphabet, mt_rand() % $alphalen, 1);
    	return $keys;
  	}

/**
* Функции, выводящая транслит руского текста на латиницу
* */
	function str2Translit($string) {
		$tmp_str = '';
		$arr_ru = "а б в г д е ё  ж  з и й к л м н о п р с т у ф х ц  ч  ш  щ   ъ ы ь э ю  я  А Б В Г Д Е Ё  Ж  З И Й  К Л М Н О П Р С Т У Ф Х Ц  Ч  Ш  Щ  Ъ  Ы Ь Э Ю  Я ";
		$arr_tr = "a b v g d e jo zh z i j k l m n o p r s t u f h ts ch sh sh' ' i ' e ju ja A B V G D E Jo Zh Z I Ji K L M N O P R S T U F H Ts Ch Sh Sh' ' I ' E Ju Ja ";
		$n = strlen($string);
		for($i=0;$i<$n;$i++) {
			$npos = strpos($arr_ru, $string[$i]);
			if(($npos === false) || ($string[$i] == Chr(32))) {
				$tmp_str .= $string[$i];
			} else {
				$tmp_str .= trim(substr($arr_tr, $npos, 2));
			}
		}
		return $tmp_str;
  	}


/**
* Функция для распарсивания URI в соответсвии с шаблоном
* Шаблон задается так :  /var/var/
* */
	function parseRouterStr($str, $rule)
	{
		$arr = explode("/", trim($str, "/") );
		$arr_rool = explode("/", trim($str, "/") );

		if(empty($arr)) return null;

		$res = array();
		foreach($arr as $k=>$v)
		{
			if( !isset($arr_rool[$k]) ) break;

			$res[$arr_rool[$k]]=$v;
		}

		return $res;
	}

	function hh($str)
	{
		return htmlspecialchars($str, ENT_QUOTES);
	}

	function w2u($str, $convert_key=false)
	{
        if( is_array($str) ){
            if( ! empty($str) ){
                foreach( $str as $k=>$v){
                    if( is_string($k) && $convert_key ){
                        $ok = $k;
                        $k = w2u($k);
                        unset($str[$ok]);
                    }
                    $str[$k] = w2u($v, $convert_key);
                }
            }
        }else{
            $str = iconv("WINDOWS-1251", "UTF-8//IGNORE", $str);
        }
		return $str;
	}


	function u2w($str, $convert_key=false)
	{
        if( is_array($str) ){
            if( ! empty($str) ){
                foreach( $str as $k=>$v){
                    if( is_string($k) && $convert_key ){
                        $k = u2w($k);
                    }
                    $str[$k] = u2w($v, $convert_key=false);
                }
            }
        }else{
            $str = @iconv("UTF-8", "WINDOWS-1251//IGNORE", $str);
        }
        return $str;
	}

    function u2u($str)
    {
        return $str;
    }


/**
 * Переводит строку в CamelCase
 * camel_case('my_data'); // вернет MyData
 */
	function camel_case($str, $ucfirst = true)
	{
		//if there are no _, why process at all
		if(strpos($str, '_') === false)
			return ($ucfirst) ? ucfirst($str) : $str;

		$items = explode('_', $str);
		$len = sizeof($items);
		$first = true;
		$res = '';
		for($i=0;$i<$len;$i++)
		{
			$item = $items[$i];
			if($item)
			{
				//we don't ucfirst first word by default
				$res .= ($first && !$ucfirst ? $item : ucfirst($item));
				$first = false;
				//skipping next "_" if it's not last
				if($i+1 < $len-1 && !$items[$i+1])
					$i++;
			}
			else
				$res .= '_';
		}

		return ($ucfirst) ? ucfirst($res) : $res;
	}


	function correct_phone($str){
		$str = preg_replace('~[^\d,]~', '', $str);
		if( empty($str) ){
		    return '';
		}
		$arr = explode(',', $str);
		$str = $arr[0];
        $ext = isset($arr[1]) ? $arr[1] : '';
		
		$length = strlen($str);

		if ($length>11) {
			$str = substr($str, 0, 11);
		}elseif($length<11){
			$str = '7'.$str;
		}
		$str = preg_replace('~^\d~', '+7', $str);
		
		if( ! empty($ext) ){
		    $str = $str .','. $ext;
		}

		return $str;
	}

	function phone_decorate($phone){
        $str = preg_replace('~[^\d,]~', '', $phone);
        $arr = explode(',', $str);

        $phone = ! empty($arr[0]) ? $arr[0] : '';
        $extra = ! empty($arr[1]) ? $arr[1] : '';
        
        if( empty($phone) ){
            return '';
        }

        $return = preg_replace('~(\d{1})(\d{3})(\d{3})(\d{2})(\d{2})~', '+$1 ($2) $3-$4-$5', $phone);

        if( ! empty($extra) ){
            $return .= ', доп. '. $extra;
        }
        return $return;
    }

	function phone_link($phone, $decorate=false, $concat='; '){
	    if( empty($phone) ){
	        return '';
	    }
	    if( false !== strpos($phone, ';') ){
            $phones = explode(';', $phone);

            $arr = [];
            foreach( $phones as $phone ){
                $arr[] = phone_link($phone);
            }
            $html = implode($concat, $arr);
            return $html;
	    }

	    $correct = correct_phone($phone);
	    if( $decorate ){
	        $phone = phone_decorate($phone);
	    }
        $html = sprintf('<a href="tel:%s" class="phone_link"><i></i><span>%s</span></a>', $correct, $phone);
        return $html;
    }




function uni_strsplit($string, $split_length=1)
{
    preg_match_all('`.`u', $string, $arr);
    $arr = array_chunk($arr[0], $split_length);
    $arr = array_map('implode', $arr);
    return $arr;
}

function barcodeTranslit($str)
{
    if( ! isset($rf) ){
        static $rf;
        static $rt;
        $rf = uni_strsplit("ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮйцукенгшщзхъфывапролджэячсмитьбю");
        $rt = uni_strsplit("QWERTYUIOP[]ASDFGHJKL;'ZXCVBNM,.qwertyuiop[]asdfghjkl;'zxcvbnm,.");
    }
    $str = str_replace($rf, $rt, trim($str));
    return strtolower($str);
}



function translit($string){
    $table = array(
        'А' => 'A',
        'Б' => 'B',
        'В' => 'V',
        'Г' => 'G',
        'Д' => 'D',
        'Е' => 'E',
        'Ё' => 'YO',
        'Ж' => 'ZH',
        'З' => 'Z',
        'И' => 'I',
        'Й' => 'J',
        'К' => 'K',
        'Л' => 'L',
        'М' => 'M',
        'Н' => 'N',
        'О' => 'O',
        'П' => 'P',
        'Р' => 'R',
        'С' => 'S',
        'Т' => 'T',
        'У' => 'U',
        'Ф' => 'F',
        'Х' => 'H',
        'Ц' => 'C',
        'Ч' => 'CH',
        'Ш' => 'SH',
        'Щ' => 'CSH',
        'Ь' => '',
        'Ы' => 'Y',
        'Ъ' => '',
        'Э' => 'E',
        'Ю' => 'YU',
        'Я' => 'YA',

        'а' => 'a',
        'б' => 'b',
        'в' => 'v',
        'г' => 'g',
        'д' => 'd',
        'е' => 'e',
        'ё' => 'yo',
        'ж' => 'zh',
        'з' => 'z',
        'и' => 'i',
        'й' => 'j',
        'к' => 'k',
        'л' => 'l',
        'м' => 'm',
        'н' => 'n',
        'о' => 'o',
        'п' => 'p',
        'р' => 'r',
        'с' => 's',
        'т' => 't',
        'у' => 'u',
        'ф' => 'f',
        'х' => 'h',
        'ц' => 'c',
        'ч' => 'ch',
        'ш' => 'sh',
        'щ' => 'csh',
        'ь' => '',
        'ы' => 'y',
        'ъ' => '',
        'э' => 'e',
        'ю' => 'yu',
        'я' => 'ya',
        ' ' => '_'
    );

    $output = str_replace(
        array_keys($table),
        array_values($table),$string
    );

    return $output;
}

function clean_bom($str){
    $str = trim(preg_replace('/\x{feff}/u', '', $str));
    return $str;
}



function car_number($string){
    $string = str_replace(' ', '', $string);
    $string = w2u(strtoupper(u2w($string)));

    $table = array(
        'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D',
        'Е' => 'E', 'Ё' => 'YO', 'Ж' => 'Zh', 'З' => 'Z', 'И' => 'I',
        'Й' => 'J', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N',
        'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T',
        'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C', 'Ч' => 'Ch',
        'Ш' => 'Sh', 'Щ' => 'Sch', 'Ь' => '\'', 'Ы' => 'Y', 'Ъ' => '\'',
        'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya'
    );

    $output = str_replace(
        array_keys($table),
        array_values($table),$string
    );

    return $output;
}

/**
 * @param $number int число чего-либо
 * @param $titles array варинаты написания для количества 1, 2 и 5
 * @return string
 */
function plural_word($number, $titles=array('комментарий','комментария','комментариев')){
    $cases = array (2, 0, 1, 1, 1, 2);
    return $titles[ ($number%100 >4 && $number%100< 20)? 2 : $cases[min($number%10, 5)] ];
}

function pw($number, $titles=array('комментарий','комментария','комментариев')){
    return plural_word($number, $titles);
}

function GUID(){
    if (function_exists('com_create_guid') === true)    {
        return trim(com_create_guid(), '{}');
    }

    return sprintf(
        '%04X%04X-%04X-%04X-%04X-%04X%04X%04X',

        mt_rand(0, 65535),
        mt_rand(0, 65535),
        mt_rand(0, 65535),
        mt_rand(16384, 20479),
        mt_rand(32768, 49151),
        mt_rand(0, 65535),
        mt_rand(0, 65535),
        mt_rand(0, 65535)
    );
}