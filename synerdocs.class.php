<?php

/**
 *
 *
 */
class Api_Synerdocs_Module{

    protected $_wsdl = 'https://testservice.synerdocs.ru/ExchangeService.svc?singlewsdl';
    protected $_location = 'https://testservice.synerdocs.ru/ExchangeService.svc';

    protected $_access_token = null;
    protected $_client = null;
    protected $_trace = [];

    public function set_error($err){

        if( ! empty($err) ){
            echo ('Error: #####################'. "\n");
            echo $err;
            echo "\n\n";
        }

        echo ('Request: #####################'. "\n");
        echo '<textarea style="width: 100%" rows="40">';
        print_r( $this->getRequest() );
        echo '</textarea>';
        echo "\n\n";

        echo ('Trace: #####################'. "\n");
        print_r( $this->getTrace() );
        echo "\n\n";
    }

    /**
     * Отдает стэк запросов
     * @return string
     */
    public function getTrace(){
        return $this->_trace;
    }

    /**
     * Отдает последний запрос
     * @return string
     */
    public function getRequest(){
        return $this->client()->__getLastRequest();
    }

    /**
     * Отдает заголовки последнего запроса
     * @return string
     */
    public function getRequestHeaders(){
        return $this->client()->__getLastRequestHeaders();
    }

    /**
     * Отдает последний ответ
     * @return string
     */
    public function getResponse(){
        return $this->client()->__getLastResponse();
    }

    /**
     * Отдает заголовки последнего ответа
     * @return string
     */
    public function getResponseHeaders(){
        return $this->client()->__getLastResponseHeaders();
    }

    /**
     * Отдает Токен авторизации
     * @return null
     */
    public function getToken(){
        return $this->_access_token;
    }

    /**
     * Отдает объект SoapClient
     * @return null|SoapClient
     */
    public function client(){
        if( empty($this->_client) ){

            $contextOptions = array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "allow_self_signed"=>true,
                )
            );
            $sslContext = stream_context_create($contextOptions);

            try {
                $this->_client = new SoapClient( $this->_wsdl, [
                    'soap_version' => SOAP_1_1,
                    'cache_wsdl' => WSDL_CACHE_BOTH, //WSDL_CACHE_NONE,
                    'stream_context' => $sslContext,
                    'trace'=>true,

                ] );

                if( ! empty($this->_location) ){
                    $this->_client->__setLocation($this->_location);
                }

            }
            catch (Exception $e){
                pp($e->getMessage());
                ppd(sprintf('<b>File:</b> %s; <b>Line:</b>%d', __FILE__, __LINE__));
            }
        }

        return $this->_client;
    }

    /**
     * Выполняет запрос к сервису
     * @param $action
     * @param $params
     * @return array|bool|mixed
     */
    public function exec($action, $params){
        $client = $this->client();
        if( ! $client ){
            return false;
        }


        try {
            $result =$client->__soapCall(
                $action,
                [$params]
            );
        }
        catch (Exception $ex)
        {
            $this->set_error($ex->getMessage());
            $this->_trace = $ex->getTrace();
            return false;
        }

        $result = object_to_array($result);

        return $result;
    }

    /**
     * Авторизация по паролю
     * @param $login
     * @param $password
     * @return bool
     */
    public function auth($login, $password){

        $params = [
            'login' => $login,
            'password' => $password,
        ];

        $result = $this->exec('Authenticate', $params);

        if( ! $result ){
            return false;
        }

        if( empty($result['AuthenticateResult']) ){
            $this->set_error('Ошибка аутентификации. Неверные логин или пароль.');
            return false;
        }

        $this->_access_token = $result['AuthenticateResult'];

        return true;
    }


    /**
     * Отдет ящики доступные авторизованному пользователю
     * @return bool
     */
    public function getBoxes(){
        $token = $this->getToken();
        $params = [
            'authToken' => $token,
        ];

        $result = $this->exec('GetBoxes', $params);
        if( ! $result ){
            return false;
        }

        if( empty($result['GetBoxesResult']['BoxInfo']) ){
            $this->set_error('Для пользователя не определен ни один ящик.');
            return false;
        }

        $boxes = $result['GetBoxesResult']['BoxInfo'];


        return isset($boxes[0]) ? $boxes : [$boxes];
    }

    /**
     * Отдает ящик синердока для указанной компании
     * @param $lp
     * @return bool
     */
    public function getBox($lp){
        $boxes = $this->getBoxes();

        if( ! $boxes ){
            return false;
        }

        $inn = $lp['inn'];
        $kpp = $lp['kpp'];

        $box = null;

        foreach( $boxes as $k=>$v ){
            if( $inn == $v['Inn'] && $kpp == $v['Kpp'] ){
                return $v;
            }
        }

        $this->set_error(sprintf('Для компании %s с ИНН:%s и КПП:%s не ящик не определен', opt($lp['u_name'], ''), $inn, $kpp));
        return false;
    }


    /**
     * Отдает адрес ящика синердока для указанной компании
     * @param $lp
     * @return bool
     */
    public function getBoxAddress($lp){
        $box = $this->getBox($lp);
        if( ! $box ){
            return false;
        }
        return $box['Address'];
    }

    public function getUserInfo($box_id){
        $token = $this->getToken();
        $params = [
            'authToken' => $token,
            'boxId' => $box_id,
        ];

        $result = $this->exec('GetUserInfo', $params);
        if( ! $result ){
            return false;
        }

        if( empty($result['GetUserInfoResult']) ){
            $this->set_error('Для пользователь по указанному токену и ящику не определен');
            return false;
        }


        return $result['GetUserInfoResult'];
    }


    public function getOrganization($box_id, $inn, $kpp){
        $token = $this->getToken();
        $params = [
            'authToken' => $token,
            'boxId' => $box_id,
            'criteria' => 'ByInnKpp',
            'values' => [
                'Inn' => $inn,
                'Kpp' => $kpp,
            ]
        ];

        $result = $this->exec('GetOrganizationBy', $params);
        if( ! $result ){
            return false;
        }

        if( empty($result['GetOrganizationByResult']) ){
            $this->set_error(sprintf('Организация с ИНН/КПП: %s/%s не найдена.', $inn, $kpp));
            return false;
        }


        return $result['GetOrganizationByResult'];
    }


}