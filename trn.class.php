<?php
include_once 'synerdocs.class.php';

/**
 *
 *
 */
class Api_Synerdocs_TrN_Module extends Api_Synerdocs_Module{

    public function generateConsignorTitle($trn){
        $data = $this->_generateConsignorTitleBySynerdocs($trn);
        return $data;
    }


    protected function _generateConsignorTitleBySynerdocs($trn){

        $token = $this->getToken();
        $data = $this->prepareConsignorTitle($trn);

        if( ! $data ){
            return false;
        }

        $params = [
            'credentials' => [
                'AuthToken' => $token
            ],
            'request' => [
                'Model' => $data,
                'Options' => [
                    // Признак необходмиости проверки корректности генерируемого контента (соответствия xml-схеме)
                    'ValidateContent' => 'true'
                ]
            ]
        ];



        $result = $this->exec('GenerateTransportWaybillConsignorTitle', $params);

        if( ! $result ){
            return false;
        }

        $Content = $result['GenerateTransportWaybillConsignorTitleResult']['GeneratedContent']['NamedContent']['Content'];
        $FileName = $result['GenerateTransportWaybillConsignorTitleResult']['GeneratedContent']['NamedContent']['Name'];

        $xml = strpos($Content, '<?xml') !== false ? $Content : base64_decode($Content);

        $return = [
            'Content' => base64_encode($xml),
            'Name' => $FileName,
            'Xml' => w2u($xml)
        ];

        return $return;
    }


    protected function _parseKey($name){
        $tmp = explode('|', $name);
        $name = $tmp[0];
        $type = ! empty($tmp[1]) ? $tmp[1] : null;
        $ns = ! empty($tmp[2]) ? $tmp[2] : null;
        return [$name, $type, $ns];
    }
    protected function _toSoapVars($data, $name){
        $soap = [];

        if( is_array($data) ){
            foreach( $data as $key=>$value ){
                $value = $this->_toSoapVars($value, $key);
                list($key, $type, $ns) = $this->_parseKey($key);
                $soap[] = new SoapVar($value, SOAP_ENC_OBJECT, $type, $ns, $key);
            }

            list($key, $type, $ns) = $this->_parseKey($name);
            return new SoapVar($soap, SOAP_ENC_OBJECT, $type, $ns, $key);
        }
        else{
            list($key, $type, $ns) = $this->_parseKey($name);
            return new SoapVar($data, XSD_STRING, $type, $ns, $key);
        }

    }

    public function prepareConsignorTitle($trn){

        $data = [];
        $_use_vars = true;
        $_lpType = '|LegalEntityCounterparty|http://schemas.datacontract.org/2004/07/Midway.ObjectModel';
        $_npType = '|IndividualCounterparty|http://schemas.datacontract.org/2004/07/Midway.ObjectModel';

        ## Версия формата документа
        ################################################################################################################
        $data['FormatVersion']['Code'] = 1;

        ## Описание сведений о сдаче груза (7 раздел ТрН)
        ################################################################################################################
        if( empty($trn['consignee']) ){
            $this->set_error('Не определено ЮЛ - Грузополучатель');
            return false;
        }

        $data['CargoDelivery']['AdditionalInfo']['InfoText']['NameCodeObject']['Code'] = 'Организация';
        $data['CargoDelivery']['AdditionalInfo']['InfoText']['NameCodeObject']['Name'] = $trn['consignee']['name'];
        // Адрес места сдачи груза
        $data['CargoDelivery']['Address']['CountryCode'] = 643;
        $data['CargoDelivery']['Address']['AddressLocationType']['Code'] = 1;
        $data['CargoDelivery']['Address']['AddressLocationType']['Name'] = 'UnstructuredAddress';
        $data['CargoDelivery']['Address']['ForeignStreetAddress'] = $trn['unloading_point']['adress'];

        ## Описание груза (3 раздел ТрН)
        ################################################################################################################
        $data['CargoDescription']['Cargo']['Name'] = implode(', ', $trn['cargo_type']);

        // Масса брутто всего груза
        $data['CargoDescription']['CargoArea']['Total']['GrossWeight']['Number'] = $trn['weight'];
        $data['CargoDescription']['CargoArea']['Total']['GrossWeight']['UnitOfMeasure']['Code'] = 166;
        $data['CargoDescription']['CargoArea']['Total']['GrossWeight']['UnitOfMeasure']['Name'] = 'кг';

        // Количество мест всего
        $data['CargoDescription']['CargoArea']['Total']['NumberOfAreas']['Number'] = $trn['pallets'];
        $data['CargoDescription']['CargoArea']['Total']['NumberOfAreas']['UnitOfMeasure']['Code'] = '0000';
        $data['CargoDescription']['CargoArea']['Total']['NumberOfAreas']['UnitOfMeasure']['Name'] = 'паллет';

        // Описание позиций груза
        // Описание упаковки груза, заполняется, как код-значение
        $data['CargoDescription']['CargoArea']['Units']['CargoAreaUnit']['ContainerType']['Code'] = 'Упаковка';
        $data['CargoDescription']['CargoArea']['Units']['CargoAreaUnit']['ContainerType']['Name'] = 'Коробка на поддонах в стрейч пленке';
        // Количество мест
        $data['CargoDescription']['CargoArea']['Units']['CargoAreaUnit']['NumberOfAreas']['Number'] = $trn['boxes'];


        ## Данные о приеме груза (6 раздел ТрН)
        ################################################################################################################
        if( empty($trn['loading_point']) ){
            $this->set_error('Не определен пункт загрузки');
            return false;
        }
        if( empty($trn['shipper']) ){
            $this->set_error('Не определено ЮЛ - грузоотправитель');
            return false;
        }

        $data['CargoReception']['AdditionalInfo']['InfoText']['NameCodeObject']['Code'] = 'Организация';
        $data['CargoReception']['AdditionalInfo']['InfoText']['NameCodeObject']['Name'] = $trn['shipper']['name'];

        if( ! empty($trn['loading_point']['phone']) ){
            $data['CargoReception']['AdditionalInfo']['InfoText']['NameCodeObject']['Code'] = 'Телефон';
            $data['CargoReception']['AdditionalInfo']['InfoText']['NameCodeObject']['Name'] = $trn['loading_point']['phone'];
        }

        $data['CargoReception']['Address']['CountryCode'] = 643;
        $data['CargoReception']['Address']['AddressLocationType']['Code'] = 1;
        $data['CargoReception']['Address']['AddressLocationType']['Name'] = 'UnstructuredAddress';
        $data['CargoReception']['Address']['ForeignStreetAddress'] = $trn['unloading_point']['adress'];

        // Состояние груза при погрузке

        if( ! empty($trn['loading_point']['stamp']) ){
            // Номер пломбы
            $data['CargoReception']['CargoState']['Cargo']['Seal'] = $trn['loading_point']['stamp'];
        }

        // Описание грузовых мест при погрузке
        // Масса брутто всего груза
        $data['CargoReception']['CargoState']['CargoArea']['Total']['GrossWeight']['Number'] = $trn['weight'];
        $data['CargoReception']['CargoState']['CargoArea']['Total']['GrossWeight']['UnitOfMeasure']['Code'] = 166;
        $data['CargoReception']['CargoState']['CargoArea']['Total']['GrossWeight']['UnitOfMeasure']['Name'] = 'кг';

        // Количество мест всего
        $data['CargoReception']['CargoState']['CargoArea']['Units']['CargoAreaUnit']['ContainerType']['Code'] = 'Упаковка';
        $data['CargoReception']['CargoState']['CargoArea']['Units']['CargoAreaUnit']['ContainerType']['Name'] = 'Коробка на поддонах в стрейч пленке';
        // Количество мест
        $data['CargoReception']['CargoState']['CargoArea']['Units']['CargoAreaUnit']['NumberOfAreas']['Number'] = $trn['boxes'];


        ## Данные о перевозчике и водителе (10 раздел ТрН)
        ################################################################################################################

        if( empty($trn['driver']) ){
            $this->set_error('Водитель не определен');
            return false;
        }
        // Информация о водителях
        $data['Carrier']['Drivers']['CarrierDriver']['Requisites']['FullName']['FirstName'] = $this->_npName( opt($trn['driver']['name'], ''), 1);
        $data['Carrier']['Drivers']['CarrierDriver']['Requisites']['FullName']['LastName'] = $this->_npName( opt($trn['driver']['name'], ''), 0);
        $data['Carrier']['Drivers']['CarrierDriver']['Requisites']['FullName']['MiddleName'] = $this->_npName( opt($trn['driver']['name'], ''), 2);

        if($_use_vars) $data['Carrier']['Drivers']['CarrierDriver']['Requisites'] = $this->_toSoapVars($data['Carrier']['Drivers']['CarrierDriver']['Requisites'], 'Requisites'.$_npType);

        // Данные о перевозчике (ТЛК)
        if( empty($trn['forwarding']) ){
            $this->set_error('Перевозчик не определен');
            return false;
        }
        $data['Carrier']['Requisites']['Inn'] = $trn['forwarding']['inn'];
        $data['Carrier']['Requisites']['OrganizationName'] = $trn['forwarding']['name'];

        if($_use_vars) $data['Carrier']['Requisites'] = $this->_toSoapVars($data['Carrier']['Requisites'], 'Requisites'.$_lpType);


        ## Данные о грузополучателе (2 раздел ТрН)
        ################################################################################################################
        $data['Consignee']['Address']['CountryCode'] = 643;
        $data['Consignee']['Address']['AddressLocationType']['Name'] = 'UnstructuredAddress';
        $data['Consignee']['Address']['AddressLocationType']['Code'] = 1;
        $data['Consignee']['Address']['ForeignStreetAddress'] = $trn['consignee']['legal_adress'];
        $data['Consignee']['Inn'] = $trn['consignee']['inn'];
        $data['Consignee']['OrganizationName'] = $trn['consignee']['name'];

        if($_use_vars) $data['Consignee'] = $this->_toSoapVars($data['Consignee'], 'Consignee'.$_lpType);


        ## Данные о грузоотправителе (1 раздел ТрН)
        ################################################################################################################
        $data['Consignor']['Address']['CountryCode'] = 643;
        $data['Consignor']['Address']['AddressLocationType']['Code'] = 1;
        $data['Consignor']['Address']['AddressLocationType']['Name'] = 'UnstructuredAddress';
        $data['Consignor']['Address']['ForeignStreetAddress'] = $trn['shipper']['legal_adress'];
        $data['Consignor']['Inn'] = $trn['shipper']['inn'];
        $data['Consignor']['OrganizationName'] = $trn['shipper']['name'];

        if($_use_vars) $data['Consignor'] = $this->_toSoapVars($data['Consignor'], 'Consignor'.$_lpType);


        ## Указания грузоотправителя(5 раздел ТрН)
        ################################################################################################################
        // Тапературный режим
        $data['ConsignorInstructions']['CarriageInstructions']['TemperatureCondition']['MaximumTemperature'] = $this->_tmpr($trn['tmpr'], 1);
        $data['ConsignorInstructions']['CarriageInstructions']['TemperatureCondition']['MinimumTemperature'] = $this->_tmpr($trn['tmpr'], 0);

        // Объявленная стоимость груза
        $data['ConsignorInstructions']['DeclaredAmount']['Total']['Text'] = Tools_SumToString::getInstance($trn['sum'])->get("[i/%s] [i~рубль|рубля|рублей~] [f/%02d] [f~копейка|копейки|копеек~]");


        ## Сопроводительные документы (4 раздел ТрН)
        ################################################################################################################
        $data['ShippingDocuments']['ShippingDocument'] = [];

        foreach( $trn['docs'] as $k=>$v ){
            $doc = [
                'Number' => $v['wb_number'],
                'Name' => 'Накладная'
            ];
            $data['ShippingDocuments']['ShippingDocument'][] = $doc; //['ShippingDocument' => $doc ];
        }

        ## Данные о транспортных средствах
        ################################################################################################################
        if( empty($trn['car']) ){
            $this->set_error('ТС не определено');
            return false;
        }

        $data['VehiclesDescription']['Vehicles']['Vehicle'] = [];
        $data['VehiclesDescription']['Vehicles']['Vehicle'][] = [
            'Brand' => opt($trn['car']['model'], ''),
            'RegistrationNumber' => opt($trn['car']['carrieres_number'], ''),
        ];
        if( ! empty($trn['rs']['trailer_number']) ){
            $data['VehiclesDescription']['Vehicles']['Vehicle'][] = [
                'RegistrationNumber' => $trn['rs']['trailer_number'],
            ];
        }

        ## Номер и дата ТрН
        ################################################################################################################
        $data['WaybillNumberDate']['Date'] = $this->_dateCorrect($trn['date']);
        $data['WaybillNumberDate']['Number'] = $trn['number'];

        return $data;
    }

    protected function _npName($name, $index){
        $arr = explode(' ', trim( preg_replace('~\s+~', ' ', $name)));
        return opt($arr[$index], '');
    }

    protected function _tmpr($tmprs, $index){
        $min = null;
        $max = null;
        $no = null;
        foreach( $tmprs as $tmpr ){
            if( ! preg_match('~[0-9]~', $tmpr) ){
                $no = $tmpr;
                continue;
            }
            $tmpr = str_replace('--', '~-', $tmpr);
            $tmp = explode('-', $tmpr);
            $tmp_min = $tmp[0]*1;
            $tmp_max = isset($tmpr[1]) ? $tmp[1]*1 : $tmp_min;

            if( null == $min || $min > $tmp_min ){
                $min = $tmp_min;
            }
            if( null == $max || $max < $tmp_max ){
                $max = $tmp_max;
            }
        }

        if( null !== $min ){
            return  $index == 0 ? $min : $max;
        }

        return $no === null ? 'Не определен' : $no;
    }

    protected function _dateCorrect($date){
        return date('Y-m-d\TH:i:s', strtotime($date));
    }
}